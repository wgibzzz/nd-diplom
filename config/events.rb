WebsocketRails::EventMap.describe do
  # В этом файле мы описываем события, на которые будет реагировать наш контроллер ChatController
  # При получении запроса от Javascript клиента
  subscribe :client_connected, :to => ChatController, :with_method => :user_connected
  subscribe :client_disconnected, :to => ChatController, :with_method => :user_disconnected
  namespace :message do
    subscribe :send, :to => ChatController, :with_method => :new_message
    subscribe :history, :to => ChatController, :with_method => :get_history
  end
  namespace :user do
    subscribe :connected, :to => ChatController, :with_method => :broadcast_connected_users
  end
  namespace :websocket_rails do
    #subscribe :pong, :to => ChatController, :with_method => :broadcast_connected_users
  end
end
