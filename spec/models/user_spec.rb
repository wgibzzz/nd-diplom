require 'spec_helper'

describe User do
  it 'has a valid factory' do
    user = create(:user)
    expect(user).to be_valid
    user.delete
  end
  it 'is invalid without email' do
    user = build(:user, email: nil)
    expect(user).to_not be_valid
    user.delete
  end
  it 'is valid without username' do
    user = build(:user, name: nil)
    expect(user).to be_valid
    user.delete
  end
  it 'is invalid without password' do
    user = build(:user, password: nil)
    expect(user).to_not be_valid
    user.delete
  end
  it 'is returns remote avatar if user has not uploaded an avatar' do
    user = build(:user)
    expect(user.avatar).to_not exist
    expect(user.remote_avatar).to be_a(String)
  end
  it 'returns an user email or username as a string' do
    user = build(:user)
    expect(user.username).to match(user.name)
  end

end
