require 'spec_helper'

describe Message do
  it 'has a valid factory' do
    user = create(:user)
    message = create(:message)
    expect(message).to be_valid
  end
  it 'is invalid without user' do
    message = build(:message, user: nil)
    expect(message).to_not be_valid
  end
  it 'is invalid without body' do
    user = create(:user)
    message = build(:message, body: nil)
    expect(message).to_not be_valid
  end
end
