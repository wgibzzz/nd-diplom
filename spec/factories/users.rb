require 'faker'

FactoryGirl.define do
  factory :user do |user|
    user.email Faker::Internet::email
    user.name Faker::Name::name
    user.password '22222222'
    user.password_confirmation { '22222222' }
  end

end