# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :message do |message|
    message.body 'Hello, RSpec!'
    message.user User.first
  end
end
