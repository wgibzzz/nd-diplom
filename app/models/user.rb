class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_reader :remote_avatar

  before_create :remote_avatar
  before_save :remote_avatar
  before_update :remote_avatar

  has_attached_file :avatar, :styles => {:medium => '300x300>', :thumb => '50x50>'}
  validates_attachment_content_type :avatar, :content_type => ['image/jpg', 'image/jpeg', 'image/png', 'image/gif']

  has_many :messages

  scope :online, lambda { where('updated_at > ?', 5.minutes.ago) }

  def username
    name ? name : email
  end

  def remote_avatar
    image_email = email.downcase
    image_hash = Digest::MD5::hexdigest(image_email)
    url = "http://www.gravatar.com/avatar/#{image_hash}.png?s=500&d=retro"
    self.avatar = URI.parse(url)

    @remote_avatar = url
  end

  def online?
    updated_at > 10.minutes.ago
  end
end
