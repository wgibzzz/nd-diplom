$(document).ready ->
  $("a[rel~=popover], .has-popover").popover()
  $("a[rel~=tooltip], .has-tooltip").tooltip()
  $("a[data-toggle='ajaxModal']").on 'click', (e) ->
    $('#ajaxModal').remove()
    e.preventDefault()
    $this = $(this)
    $remote = $this.data('remote') || $this.attr('href')
    $modal = $('<div class="modal" id="ajaxModal"><div class="modal-body"></div></div>');
    $('body').append($modal)
    $modal.modal()
    $modal.load($remote)
    return

  return