# По сути тут находится большая часть логики приложения
class NDChat
  # Метод, вызываемый при создании экземпляра класса
  constructor: (uri) ->
    # Устанавливаем соединение с сервером
    @dispatcher = new WebSocketRails(uri, true)
    # Привязываем события
    @bindEvents()
    @onlineUsers = []
    return
  # Привязываем методы класса на ответы от WebSockets сервера а так же на события jQuery
  bindEvents: =>
    # Ответ с данными нового пользователя
    @dispatcher.bind 'user.connected', @userConnected
    @dispatcher.bind 'user.disconnected', @userDisconnected
    # Ответ с новым сообщение
    @dispatcher.bind 'message.new', @newMessage
    # Начальная подгрузка истории
    @dispatcher.bind 'messages.history', @messagesHistory
    # Ответ со списком пользователей в онлайне
    @dispatcher.bind 'user.online', @updateOnlineUsers
    # Колбэк-функция отправки сообщения на события клика или нажатия клавиши\сочетания клавиш
    $('.message-form button').on 'click', @sendMessage
    # Тут проверка на то, что поле находится в фокусе, т.е. юзер нажал на него
    if $('#message-body:focus')
      # И если условие возвращает булево true, то мы биндим колбэк на нажатие сочетания клавиш\нажатие клавиши
      jwerty.key 'ctrl+enter/enter', @sendMessage
    # Отправляем на сервер запрос на получеие истории
    @dispatcher.trigger 'message.history'

    return
  # Колбэк-функция, отправляющая новое сообщение на сервер
  sendMessage: (event) =>
    # jQuery power
    # Задаем область поиска
    $messageForm = $('.message-form')
    # Инпут, в который вводится сообщение
    $messageBody = $messageForm.find('#message-body')
    # Скрытый инпут с ид юзера. Подмена не прокатит, поскольку мы проверяем на совпадение данных на серверной стороне
    $messageUser = $messageForm.find('#message-user').val()
    # Формируем массив с данными сообщения
    message = {
      body: $messageBody.val(),
      user_id: $messageUser
    }
    # И отправляем его
    @dispatcher.trigger 'message.send', message
    # Очищаем инпут и "кликаем" на него
    $messageBody.val('')
    $messageBody.focus()
    # Отменяем дефолтное действие, чтобы страницу не перезагружало при клике на кнопку
    event.preventDefault()
    return
  # Обработка ответа сервера, в котором пришло новое сообщение
  newMessage: (data) =>
    $('#chat .jspPane').append @newMessageTemplate(data)
    return
  # Обработка ответа с массивом последних 20 сообщений в чате
  # Принцип схож с методом newMessage
  messagesHistory: (data) =>
    $('#chat').spin(false)
    # Только тут мы проходимся по каждому элементу массива при помощи $.each()
    chatPane = document.getElementById('chat').getElementsByClassName('jspPane')[0]
    for element in data
      chatPane.innerHTML += @newMessageTemplate element
    return
  updateOnlineUsers: (usersArray) =>
    for user in usersArray
      if ($('.well.user[data-uid=' + user.id + ']').length > 0)
        return
      else
        $('#users-online').append @userPanelTemplate(user)
    console.log('online update')
    return
  userConnected: (user) =>
    if ($('.well.user[data-uid=' + user.id + ']').length > 0)
      return
    else
      $('#users-online').append @userPanelTemplate(user)
    return
  # remove user pane
  userDisconnected: (user) =>
    $('.well.user[data-uid=' + user.id + ']').slideUp(300);
    $('.well.user[data-uid=' + user.id + ']').delay(300).remove();
    return
  # Обработка обьекта с сообщением и юзером, возвращающая готовое к присоединению DOM дерево
  newMessageTemplate: (data) ->
    # запихиваем данные в HTML
    html = """
<div class='message'>
  <div class='row'>
    <div class='col-md-2 message-info'>
      <img src='#{data.user.avatar}' class='img-thumbnail img-rounded img-responsive'>
    </div>
    <div class='col-md-10 message-body'>
      <div class='message-meta'>
        <a href='#{data.user.link}' class='label label-info'>#{data.user.name}</a>
        <span class='badge'>#{data.message.created_at}</span>
      </div>
      #{
      #@TODO: implement message styling and bbCode (unnecessary)
      data.message.body
      }
    </div>
  </div>
</div>
"""
    # Возвращаем ноду
    return html
  userPanelTemplate: (data) ->
    html = """
<div class='well well-sm user' data-uid='#{data.id}'>
  <div class='media'>
    <a href='#{data.link}' class='pull-left'>
      <img src="#{data.avatar}" class='img-rounded img-thumbnail media-object'>
    </a>
    <div class='media-body'>
      <div class='media-heading'>
        <a href='#{data.link}'>#{data.name}</a>
      </div>
    </div>
  </div>
</div>
"""
    return html
# Обработка всего остального и создание экземпляра класса чата будет происходить при загрузке страницы
# в нижеследующей анонимной функции, которая вызывает саму себя, после декларации
$ ->
  # Создание экземпляра класса
  window.NDChat = new NDChat($('#chat').data('uri'))
  # Заглушка-крутилка, пока не получим историю
  $('#chat').spin({
    lines: 12, # Параметры, отвечающие за внешний вид
    length: 0,
    width: 16,
    radius: 45,
    corners: 1,
    rotate: 0,
    direction: 1,
    color: 'rgba(0,0,0,0.5)',
    speed: 1,
    trail: 100,
    shadow: true,
    hwaccel: false,
    className: 'spinner',
    zIndex: 2e9,
    top: '50%',
    left: '50%'
  })
  # Библиотека красивой прокрутки внутри контейнера.
  # Выбираем контейнер
  chatWindow = $('#chat')
  # инициализируем прокрутку
  chatWindow.jScrollPane({
    autoReinitialise: true,
    animateScroll: true
  })
  # API этой библиотеки позволяет выполнять прокрутку к любому месту или элементу в контейнере.
  # Получаем инстанс прокрутки
  chatWindowApi = chatWindow.data('jsp')
  # и привязываем обработчик события изменения дерева DOM внутри контейнера прокрутки
  $('#chat .jspPane').on 'DOMSubtreeModified', ->
    # Поскольку первый раз дерево внутри контейнера изменяется после включения заглушки-крутилки,
    # мы не сможем прокрутить к последнему сообщению, ибо их нет. Соотв., интерпретатор JavaScript будет ругаться.
    # Ставим защиту - проверку количества сообщений.
    if chatWindow.find('.message').length > 0
      # И если значение больше нуля, прокручиваем к последнему сообщению
      chatWindowApi.scrollToElement($('#chat .message:last'))
    return false
  # CKEditor
  $('#message-body').ckeditor()
  # Вешаем проверку на событие отжатия конпки клавиатуры или изменение. Проверка на присутствие символа.
  $('#message-body').on 'change keyup', ->
    $val = $(this).val()
    # Если значение
    if $val != ''
      # не равно нулю, активируем кнопку
      $('#message-send').removeAttr 'disabled'
    else
      # иначе, деактивируем кнопку
      $('#message-send').attr 'disabled', 'disabled'
    return
  return