class ChatController < WebsocketRails::BaseController
  include ActionView::Helpers::SanitizeHelper

  def user_connected
    unless current_user.nil?
      @user = generate_user_json current_user
      broadcast_message 'user.connected', @user
      broadcast_connected_users
    end
  end

  def user_disconnected
    unless current_user.nil?
      @user = generate_user_json current_user
      broadcast_message 'user.disconnected', @user
    end
  end

  def new_message
    @message = Message.new(body: sanitize(message[:body]), user_id: message[:user_id])
    puts @message
    if @message.save
      data = generate_message_json @message
      User.find(@message.user.id).touch
      broadcast_message 'message.new', data
    else
      puts @message.errors.as_json
    end
  end

  def get_history
    @messages = []
    Message.includes(:user).last(20).each do |message|
      json = generate_message_json message
      @messages.push json
    end
    broadcast_message 'messages.history', @messages
  end

  def broadcast_connected_users
    @online_users = []
    User.online.each { |user| @online_users.push generate_user_json user }
    broadcast_message 'user.online', @online_users
  end

  def generate_message_json(data)
    avatar = data.user.avatar.exists? ? data.user.avatar.url(:medium) : data.user.remote_avatar
    {
        :message => {
            :id => data.id,
            :body => sanitize(data.body),
            :created_at => l(data.created_at, :format => :long)
        },
        :user => {
            :id => data.user.id,
            :name => data.user.username,
            :avatar => avatar,
            :link => user_path(data.user)
        }
    }
  end

  def generate_user_json(data)
    {
        :id => data.id,
        :avatar => data.avatar.exists? ? data.avatar.url(:thumb) : data.avatar.remote_url,
        :name => data.username,
        :link => user_path(data.id),
        :quote => "!@user-#{data.id}"
    }
  end
end