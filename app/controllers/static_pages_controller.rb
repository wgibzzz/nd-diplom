class StaticPagesController < ApplicationController
  before_filter :authenticate_user!, :only =>  [:chat]

  def index

  end

  def chat
    render :layout => 'chat'

  end

  def speedtest

  end

  def user_modal
    unless current_user.nil?
      @last_messages = Message.order(:created_at => :asc).where(:user_id => current_user.id).limit(6)
    end
    render 'chunks/user_modal', :layout => false
  end

end
